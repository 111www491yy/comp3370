from re import A
from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        prevFood = currentGameState.getFood()
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        "*** YOUR CODE HERE ***"
        base = 0

        food = newFood.asList()
        if(prevFood[newPos[0]][newPos[1]]):
          base += 50

        fooddistance = []
        for pos in food:
            fooddistance.append(manhattanDistance(newPos,pos))
        if(len(fooddistance)):
          shortestfood = min(fooddistance)
        else:
          return successorGameState.getScore()

        ghost = successorGameState.getGhostPositions()
        ghostdistance = []
        for pos in ghost:
            ghostdistance.append(manhattanDistance(newPos,pos))
        if(len(ghostdistance)):
          shortestghost = min(ghostdistance)
        else:
          shortestghost = currentGameState.getWalls().height
        if shortestghost < 2:
          base -= 500

        base = base + 1/shortestfood + 0.3*shortestghost/(currentGameState.getWalls().height+currentGameState.getWalls().width)

        return  base
        

def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game

          gameState.isWin():
            Returns whether or not the game state is a winning state

          gameState.isLose():
            Returns whether or not the game state is a losing state
        """
        "*** YOUR CODE HERE ***"
        def maxer(gameState,d):

          if(d ==self.depth or gameState.isWin() or gameState.isLose()):
            return self.evaluationFunction(gameState)

          v = float('-inf')

          for action in gameState.getLegalActions(0):
            v = max(v, miner(gameState.generateSuccessor(0, action),d,1))

          return v

        def miner(gameState,d, agent):

          if(d ==self.depth or gameState.isWin() or gameState.isLose()):
            return self.evaluationFunction(gameState)

          v = float('inf')

          no_agents = gameState.getNumAgents()-1
          if(agent == no_agents):
            for action in gameState.getLegalActions(agent):
              v = min(v,maxer(gameState.generateSuccessor(agent, action),d+1))

          else:
            for action in gameState.getLegalActions(agent):
              v = min(v,miner(gameState.generateSuccessor(agent, action),d,agent+1))

          return v
        move = Directions.STOP
        v = float('-inf')
        for action in gameState.getLegalActions(0):
            actionvalue = miner(gameState.generateSuccessor(0, action),0,1)
            if actionvalue>v:
              move = action
              v = actionvalue
        return move    
        util.raiseNotDefined()

class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"
        def maxer(gameState,d,a,b):

          if(d ==self.depth or gameState.isWin() or gameState.isLose()):
            return self.evaluationFunction(gameState)

          v = float('-inf')

          for action in gameState.getLegalActions(0):
            v = max(v, miner(gameState.generateSuccessor(0, action),d,1,a,b))
            if(v>b):
              return v
            a = max(a,v)
          return v

        def miner(gameState,d, agent,a,b):

          if(d ==self.depth or gameState.isWin() or gameState.isLose()):
            return self.evaluationFunction(gameState)

          v = float('inf')

          no_agents = gameState.getNumAgents()-1
          if(agent == no_agents):
            for action in gameState.getLegalActions(agent):
              v = min(v,maxer(gameState.generateSuccessor(agent, action),d+1,a,b))
              if(v<a):
                return v
              b = min(b,v)

          else:
            for action in gameState.getLegalActions(agent):
              v = min(v,miner(gameState.generateSuccessor(agent, action),d,agent+1,a,b))
              if(v<a):
                return v
              b = min(b,v)

          return v
        move = Directions.STOP
        v = float('-inf')
        a = float('-inf')
        b = float('inf')
        for action in gameState.getLegalActions(0):
          actionvalue = miner(gameState.generateSuccessor(0, action),0,1,a,b)
          if actionvalue>v:
            move = action
            v = actionvalue
          a = max(a,v)
        return move    
        util.raiseNotDefined()
        

class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction

          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """
        "*** YOUR CODE HERE ***"
        def maxer(gameState,d):

          if(d ==self.depth or gameState.isWin() or gameState.isLose()):
            return self.evaluationFunction(gameState)

          v = float('-inf')

          for action in gameState.getLegalActions(0):
            v = max(v, expect(gameState.generateSuccessor(0, action),d,1))

          return v

        def expect(gameState,d, agent):

          if(d ==self.depth or gameState.isWin() or gameState.isLose()):
            return self.evaluationFunction(gameState)

          v = 0

          no_agents = gameState.getNumAgents()-1
          if(agent == no_agents):
            for action in gameState.getLegalActions(agent):
              v += maxer(gameState.generateSuccessor(agent, action),d+1)

          else:
            for action in gameState.getLegalActions(agent):
              v += expect(gameState.generateSuccessor(agent, action),d,agent+1)
          v = v/len(gameState.getLegalActions(agent))
          return v
        move = Directions.STOP
        v = float('-inf')
        for action in gameState.getLegalActions(0):
            actionvalue = expect(gameState.generateSuccessor(0, action),0,1)
            if actionvalue>v:
              move = action
              v = actionvalue
        return move    
        util.raiseNotDefined()

def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5).

      DESCRIPTION: <write something here so we know what you did>
      This function take 3 values for linear combination, 
      which is the number of food left and the distance to the nearest food and scared ghost.
      Here number of food has weight larger than 1, 
      and the distance to the nearest food is inversely proportioned with weight smaller than 1,
      while the distance to the nearest scared ghost is positively proportioned with weight larger than 1.
      It also gain score when a scared ghost is ate or a capsule is ate.
      It will decrease score when it is too close to normal ghost
    """
    "*** YOUR CODE HERE ***"
    currentFood = currentGameState.getFood()
    currentPos = currentGameState.getPacmanPosition()
    currentGhostStates = currentGameState.getGhostStates()
    currentScaredTimes = [ghostState.scaredTimer for ghostState in currentGhostStates]

    base = 0

    food = currentFood.asList()

    """
    get the distance to nearest food
    """
    fooddistance = []
    for pos in food:
      fooddistance.append(manhattanDistance(currentPos,pos))
    if(len(fooddistance)):
      shortestfood = min(fooddistance)
    else:
      shortestfood = 1

    """
    get the distance to ghost
    """
    ghostdistance = []
    scareddistance = []
    for ghost in currentGhostStates:
      d = manhattanDistance(currentPos,ghost.configuration.getPosition())
      if(ghost.scaredTimer):
        scareddistance.append(d)
        if(d ==0):
          base += 1000
      else:
        ghostdistance.append(d)

    if(len(scareddistance)):
      shortestscared = min(scareddistance)
      base += 500/(shortestscared+1)
    if(len(ghostdistance)):
      shortestghost = min(ghostdistance)
    else:
      shortestghost = currentGameState.getWalls().height+currentGameState.getWalls().width
    if shortestghost < 2:
      base -= 1000
    
    """
    gain score when eating capsules
    """
    cap = currentGameState.getCapsules()
    capdistance = []
    for c in cap:
      capdistance.append(manhattanDistance(currentPos,c))
      if((c[0]==currentPos[0]) and (c[1]==currentPos[1])):
        base += 1000
    
    base = base - 20*len(food) + 0.7/shortestfood
    return  base

# Abbreviation
better = betterEvaluationFunction

