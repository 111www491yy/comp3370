"""
Here's the intended output of this script, once you fill it in:

Welcome to shop1 fruit shop
Welcome to shop2 fruit shop
For orders:  [('apples', 1.0), ('oranges', 3.0)] best shop is shop1
For orders:  [('apples', 3.0)] best shop is shop2
"""

import shop

def shopSmart(orderList, fruitShops):
    """
        orderList: List of (fruit, numPound) tuples
        fruitShops: List of FruitShops
    """    
    "*** YOUR CODE HERE ***"
    finshop = None
    minprice = None
    for s in fruitShops:
        price = s.getPriceOfOrder(orderList)
        if(minprice is None):
            minprice = price
            finshop = s
        elif(price < minprice):
            minprice = price
            finshop = s
    return finshop
    
def shopArbitrage(orderList, fruitShops):
    """
    input: 
        orderList: List of (fruit, numPound) tuples
        fruitShops: List of FruitShops
    output:
        maximum profit in amount
    """
    "*** YOUR CODE HERE ***"
    total = 0
    for f,s in orderList:
        max = 0
        min = 0
        for sh in fruitShops:
            price = sh.getCostPerPound(f)
            if(max ==0 or price>max):
                max = price
            if(min == 0 or price<min):
                min = price
        total = total + (max -min)*s
    return total

def shopMinimum(orderList, fruitShops):
    """
    input: 
        orderList: List of (fruit, numPound) tuples
        fruitShops: List of FruitShops
    output:
        Minimun cost of buying the fruits in orderList
    """
    "*** YOUR CODE HERE ***"
    total = 0
    for f,s in orderList:
        min = 0
        for sh in fruitShops:
            price = sh.getCostPerPound(f)
            if(min == 0 or price<min):
                min = price
        total = total + min*s
    return total

if __name__ == '__main__':
  "This code runs when you invoke the script from the command line"
  orders = [('apples',1.0), ('oranges',3.0)]
  dir1 = {'apples': 2.0, 'oranges':1.0}
  shop1 =  shop.FruitShop('shop1',dir1)
  dir2 = {'apples': 1.0, 'oranges': 5.0}
  shop2 = shop.FruitShop('shop2',dir2)
  shops = [shop1, shop2]
  print("For orders ", orders, ", the best shop is", shopSmart(orders, shops).getName())
  orders = [('apples',3.0)]
  print("For orders: ", orders, ", the best shop is", shopSmart(orders, shops).getName())
